/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.zongtui.web.modules.monitor.dao;

import com.zongtui.web.common.persistence.CrudDao;
import com.zongtui.web.common.persistence.annotation.MyBatisDao;
import com.zongtui.web.modules.monitor.entity.MonitorCrawlerClient;

/**
 * 客户端监控DAO接口
 * @author zhangfeng
 * @version 2015-05-02
 */
@MyBatisDao
public interface MonitorCrawlerClientDao extends CrudDao<MonitorCrawlerClient> {
	
}