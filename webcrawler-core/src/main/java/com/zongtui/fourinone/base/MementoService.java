package com.zongtui.fourinone.base;

import com.zongtui.fourinone.utils.log.LogUtil;

import java.rmi.server.RemoteServer;
import java.rmi.server.ServerNotActiveException;

public class MementoService{
	public String getClientHost(){
		String clienthost=null;
		try{
			clienthost = RemoteServer.getClientHost();
		}catch(ServerNotActiveException ex){
			LogUtil.fine("[MementoService]", "[getClientHost]", ex);
		}
		//System.out.println("clienthost:"+clienthost);
		return clienthost;
	}
}