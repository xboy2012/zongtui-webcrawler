package com.zongtui.fourinone.coolhash;
public interface CoolKeyResult{
	public CoolHashMap.CoolKeySet nextBatchKey(int batchLength);
}